import React, { useState } from "react";
import axios from "axios";
import {
  Center,
  Heading,
  Code,
  Container,
  Button,
  Grid,
  GridItem,
} from "@chakra-ui/react";
import { QuoteData } from "../models/QuoteData";

const Quote = () => {
  const [quote, setQuote] = useState<QuoteData>();

  const onClickHandler = () => {
    axios
      .get<QuoteData>("http://localhost:5141/api/quotes", {
        headers: { "Access-Control-Allow-Origin": "*" },
      })
      .then((response) => {
        console.log(response);
        setQuote(response.data);
      });
  };

  return (
    <Container
      display="flex"
      alignItems="center"
      justifyContent="center"
      h="100vh"
    >
      <Grid templateRows="repeat(3)" gap={4}>
        <GridItem>
          <Center h="100px" color="white">
            <Heading as="h2" size="lg" noOfLines={1} color="#1A202C">
              DevOps - CI/CD 2023 - 193185
            </Heading>
          </Center>
        </GridItem>
        <GridItem height={10}>
          <Center>
            {quote != undefined ? (
              // quote.quoteText
              <Code colorScheme="red">{quote.quoteText}</Code>
            ) : (
              <Code colorScheme="red" children="bool IsDevOpsEasy = !yes" />
            )}
          </Center>
        </GridItem>
        <GridItem>
          <Center>
            <Button
              bg="#4299E1"
              color="white"
              size="sm"
              onClick={onClickHandler}
            >
              Get random quote
            </Button>
          </Center>
        </GridItem>
      </Grid>
    </Container>
  );
};

export default Quote;
