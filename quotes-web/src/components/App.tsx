import React from "react";
import { ChakraProvider } from "@chakra-ui/react";
import Quote from "./Quote";

function App() {
  return (
    <ChakraProvider>
      <Quote />
    </ChakraProvider>
  );
}

export default App;
