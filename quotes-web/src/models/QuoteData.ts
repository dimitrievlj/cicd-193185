export interface QuoteData {
  id: string;
  quoteText: string;
}
