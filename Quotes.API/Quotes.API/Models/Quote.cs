using System.ComponentModel.DataAnnotations;

namespace Quotes.API.Models;

public class Quote
{
    [Key]
    public string Id { get; set; }
    public string QuoteText { get; set; }
}