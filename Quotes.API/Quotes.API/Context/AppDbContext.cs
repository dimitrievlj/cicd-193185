using Microsoft.EntityFrameworkCore;
using Quotes.API.Models;

namespace Quotes.API.Context;

public class AppDbContext : DbContext
{
    protected readonly IConfiguration Configuration;

    public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
    {
    }
    
    public DbSet<Quote> Quotes { get; set; }
}