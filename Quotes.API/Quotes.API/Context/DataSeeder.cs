using Quotes.API.Models;

namespace Quotes.API.Context;

public class DataSeeder
{
    private readonly AppDbContext _dbContext;

    public DataSeeder(AppDbContext context)
    {
        _dbContext = context;
    }

    public void SeedData()
    {
        if (_dbContext.Quotes.Any()) return;

        var quotes = new List<Quote>
        {
            new Quote
            {
                Id = "dadb661e-8838-4cba-9f6d-ea440adb85b7",
                QuoteText = "First, solve the problem. Then, write the code."
            },
            new Quote
            {
                Id = "16935664-9a18-440e-96fa-16803b351bba",
                QuoteText = "Make it work, make it right, make it fast."
            },
            new Quote
            {
                Id = "510dd4f6-65ed-497e-902e-5a50ec285011",
                QuoteText = "Confusion is part of programming."
            },
            new Quote
            {
                Id = "5ff1933c-2231-43a4-9a86-df1645c4dc82",
                QuoteText = "There is always one more bug to fix. "
            },
            new Quote
            {
                Id = "6a5ca18e-5241-488a-b37f-9cefff61b600",
                QuoteText = "If, at first, you do not succeed, call it version 1.0."
            },
        };

        _dbContext.Quotes.AddRange(quotes);
        _dbContext.SaveChanges();
    }
}