using Microsoft.EntityFrameworkCore;
using Quotes.API.Context;
using Quotes.API.Interfaces;
using Quotes.API.Services;

var builder = WebApplication.CreateBuilder(args);

builder.Configuration.AddJsonFile("appsettings.json");

builder.Services.AddControllers();

var connectionString = builder.Configuration.GetConnectionString("QuotesDb");
builder.Services.AddDbContext<AppDbContext>(options =>
    options.UseNpgsql(connectionString));

builder.Services.AddTransient<DataSeeder>();
builder.Services.AddScoped<IQuoteService, QuoteService>();

var app = builder.Build();

// Run the data seeder
using (var scope = app.Services.CreateScope())
{
    var context = scope.ServiceProvider.GetService<AppDbContext>();
    context?.Database.Migrate();

    var seeder = scope.ServiceProvider.GetRequiredService<DataSeeder>();
    seeder.SeedData();
}

app.UseRouting();

app.UseCors(x => x
    .AllowAnyMethod()
    .AllowAnyHeader()
    .SetIsOriginAllowed(origin => true) // allow any origin
    .AllowCredentials()); // allow credentials

app.UseCors();

app.MapControllers(); // Map controller endpoints

app.Run();