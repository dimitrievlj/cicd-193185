using Microsoft.AspNetCore.Mvc;
using Quotes.API.Dtos;
using Quotes.API.Interfaces;

namespace Quotes.API.Controllers;

[Route("/api/quotes")]
[ApiController]
public class QuotesController : Controller
{
    private readonly IQuoteService _quoteService;

    public QuotesController(IQuoteService quoteService)
    {
        _quoteService = quoteService;
    }

    /// <summary>
    /// Returns random quote
    /// </summary>
    [HttpGet]
    public async Task<ActionResult<QuoteDto>> GetRandomQuote()
    {
        var quoteDto = await _quoteService.GetRandomQuote();
        return Ok(quoteDto);
    }
}