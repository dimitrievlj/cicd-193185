namespace Quotes.API.Dtos;

public class QuoteDto
{
    public string Id { get; set; }
    public string QuoteText { get; set; }
}