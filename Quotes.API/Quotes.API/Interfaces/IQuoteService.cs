using Quotes.API.Dtos;

namespace Quotes.API.Interfaces;

public interface IQuoteService
{
    Task<QuoteDto> GetRandomQuote();
}