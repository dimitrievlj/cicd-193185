using Microsoft.EntityFrameworkCore;
using Quotes.API.Context;
using Quotes.API.Dtos;
using Quotes.API.Interfaces;

namespace Quotes.API.Services;

public class QuoteService : IQuoteService
{
    private readonly AppDbContext _dbContext;

    public QuoteService(AppDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task<QuoteDto> GetRandomQuote()
    {
        var random = new Random();
        var randomIndex = random.Next(_dbContext.Quotes.Count());
        var quote = await _dbContext.Quotes.Skip(randomIndex).FirstOrDefaultAsync();

        return new QuoteDto
        {
            Id = quote!.Id,
            QuoteText = quote.QuoteText
        };
    }
}